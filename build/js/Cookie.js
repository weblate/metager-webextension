/**
 * Parses a "Set-Cookie" header coming from a browser
 * to give easy access to all necessary parameters of it
 */
export class Cookie {
    key = null;
    value = null;
    expired = false;
    constructor(cookieString) {
        // Return an empty object if cookieString
        // is empty
        if (cookieString === "")
            throw new Error("Cannot parse cookie string " + cookieString);

        // Get each individual key-value pairs
        // from the cookie string
        // This returns a new array
        let pairs = cookieString.split(";");

        // Separate keys from values in each pair string
        // Returns a new array which looks like
        // [[key1,value1], [key2,value2], ...]
        let splittedPairs = pairs.map(cookie => cookie.split("="));


        let expiration = null;
        for (const [index, pair] of splittedPairs.entries()) {
            if (index == 0) {
                this.key = pair[0].trim();
                this.value = decodeURIComponent(pair[1].trim());
            } else {
                let key = pair[0].trim();
                if (key.match(/^expires$/i) && this.expiration == null) {
                    expiration = new Date(pair[1].trim());
                } else if (key.match(/^max-age$/i)) {
                    let maxAge = parseInt(pair[1].trim());
                    if (maxAge == 0) {
                        this.expired = true;
                    } else {
                        expiration = new Date((new Date()).getTime() + maxAge * 1000);
                    }
                }
            }
        };

        if (expiration != null && Date.now() > expiration.getTime()) {
            this.expired = true;
        }
    }

    static async COOKIES_TO_SETTINGS() {
        let cookie_settings = {};
        // First fetch all cookies from metager.org
        await browser.cookies.getAll({ url: "https://metager.org/" }).then(async cookies => {
            for (let cookie of cookies) {
                cookie_settings[cookie.name] = cookie.value;
                await browser.cookies.remove({ name: cookie.name, url: "https://metager.org/" });
            }
        });
        await browser.cookies.getAll({ url: "https://metager.de/" }).then(async cookies => {
            for (let cookie of cookies) {
                cookie_settings[cookie.name] = cookie.value;
                await browser.cookies.remove({ name: cookie.name, url: "https://metager.de/" });
            }
        });

        // Get the current settings and overwrite the values from existing cookies
        return browser.storage.sync.get({ settings_settings: {} }).then(storage => {
            let settings = storage.settings_settings;
            for (let key in cookie_settings) {
                settings[key] = cookie_settings[key];
            }
            return browser.storage.sync.set({ settings_settings: settings });
        });
    }

    static async SETTINGS_TO_COOKIES() {
        return browser.storage.sync.get({ settings_settings: {} }).then(async storage => {
            for (let key in storage.settings_settings) {
                let value = storage.settings_settings[key];
                let expiration = new Date();
                expiration.setFullYear(expiration.getFullYear() + 1);
                await browser.cookies.set({
                    url: "https://metager.de/",
                    expirationDate: expiration.getTime(),
                    secure: true,
                    name: key,
                    value: value
                });
                await browser.cookies.set({
                    url: "https://metager.org/",
                    expirationDate: expiration.getTime(),
                    name: key,
                    value: value
                });
            }
        });
    }
}