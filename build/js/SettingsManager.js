import { Cookie } from "./Cookie";

/**
 * Handles changes in the search settings when the user
 * makes them on our Website. It will catch the regular responses 
 * which would store the settings in form of cookies and stores
 * them within this web-extension instead.
 * Also it will attach settings that are stored in this extension
 * and attaches them to requests made to our website
 */
export class SettingsManager {
  initialized = false;

  store_settings = true;
  settings = {};

  constructor() { }

  async init() {
    if (this.initialized) return;
    return browser.storage.sync
      .get({ settings_store: true, settings_settings: {} })
      .then((stored_settings) => {
        this.store_settings = stored_settings.settings_store;
        this.settings = stored_settings.settings_settings;
        this.initialized = true;
        return this;
      });
  }

  handleStorageChange(changes) {
    if (
      changes.hasOwnProperty("settings_store") &&
      typeof changes.settings_store.newValue == "boolean"
    ) {
      this.store_settings = changes.settings_store.newValue;
    }
    if (changes.hasOwnProperty("settings_settings")) {
      this.settings = changes.settings_settings.newValue;
    }
  }

  async handleRequestHeaders(details) {
    if (!this.store_settings) return details.requestHeaders;
    await this.init();
    let cookies = [];

    let headers = [];
    // Include any possibly existing cookies
    for (let header of details.requestHeaders) {
      if (header.name.match(/cookie/i)) {
        let existing_cookies = header.value.split(";");
        for (let cookie of existing_cookies) {
          cookies.push(cookie.trim());
        }
      } else {
        headers.push(header);
      }
    }

    for (let key in this.settings) {
      cookies.push(`${key}=${encodeURIComponent(this.settings[key])}`);
    }

    if (cookies.length > 0) {
      let cookie_header = {
        name: "Cookie",
        value: cookies.join("; "),
      };
      headers.push(cookie_header);
    }
    return headers;
  }

  /**
   *
   * @param {*} details
   */
  async handleResponseHeaders(details) {
    if (!this.store_settings) return details.responseHeaders;
    await this.init();
    if (details.hasOwnProperty("responseHeaders")) {
      let new_headers = []; // Cookie Array without set-cookie
      let settings_changed = false;
      for (const header of details.responseHeaders) {
        if (header.name.match(/set-cookie/i)) {
          let cookie_responses = header.value.split("\n");
          let new_cookies = [];
          for (let cookie of cookie_responses) {
            let response_cookie = new Cookie(cookie);

            if (response_cookie.expired) {
              new_cookies.push(cookie);
              delete this.settings[response_cookie.key];
            } else if (response_cookie.key == "cost" || response_cookie.key == "tokens") {
              // Do not remove cost cookie as the token manager uses it
              new_cookies.push(cookie);
              continue;
            } else {
              this.settings[response_cookie.key] = decodeURIComponent(
                response_cookie.value
              );
            }
          }
          if (new_cookies.length > 0) {
            header.value = new_cookies.join("\n");
            new_headers.push(header);
          }
          settings_changed = true;
        } else {
          new_headers.push(header);
        }
      }
      if (settings_changed) {
        browser.storage.sync.set({ settings_settings: this.settings });
      }
      return new_headers;
    }
  }
}
