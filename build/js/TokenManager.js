import { blind, unblind, verify } from "blind-signatures";
import NodeRSA from "node-rsa";
import { Cookie } from "./Cookie";

/**
 * If the user is using a MetaGer Key (https://metager.org/keys)
 * to use our search engine without advertisements he can decide
 * to use anonymous token (https://metager.org/keys/help/anonymous-token)
 * instead of his key to authorize his requests to our server.
 * 
 * Homomorphic encryption is used to generate signed authorization tokens
 * which are then impossible for us to connect with the used MetaGer
 * Key.
 * 
 * The Tokenmanager is responsible for making sure that the configured MetaGer
 * Key is no longer transmitted to our server. Also it will generate anonymous token
 * as required and attach them to the search requests as needed.
 */
export class TokenManager {
  _storage_keys = {
    anonymous_tokens_enabled: "use_anonymous_tokens",
    key_charge: "anonymous_tokens_key_charge",
    tokens: "anonymous_tokens",
    tokens_recent_costs: "anonymous_tokens_recent_costs",
    settings: "settings_settings",
  };

  _initialized = false;
  _anonymous_tokens_enabled = false;
  _key = null;
  _key_charge = 0;
  _tokens_attach = false; // Attach tokens to result page
  _tokens = []; // Stored Tokens

  _api_base = "https://metager.de/keys/api/json";

  // Settings related to calculating how many tokens to locally store in the extension
  // Will be the maximum cost of a single search plus the cost of the latest searches
  _recent_costs = []; // The latest cost for the last few MetaGer searches
  _recent_costs_number = 4; // How many recent costs to account for in calculation

  constructor() {
    this.init();
  }

  async init() {
    if (this._initialized) return;
    // Initialize Settings
    let settings = {};
    settings[this._storage_keys.anonymous_tokens_enabled] = false;
    settings[this._storage_keys.settings] = {};

    return browser.storage.sync
      .get(settings)
      .then((storage) => {
        this._anonymous_tokens_enabled =
          storage[this._storage_keys.anonymous_tokens_enabled];
        if (storage[this._storage_keys.settings].hasOwnProperty("key")) {
          this._key = storage[this._storage_keys.settings].key;
        }
        settings = {};
        settings[this._storage_keys.tokens] = [];
        settings[this._storage_keys.tokens_recent_costs] = [1];
        settings[this._storage_keys.key_charge] = 0;
        return browser.storage.local.get(settings);
      })
      .then((storage) => {
        this._tokens = storage[this._storage_keys.tokens];
        this._recent_costs = storage[this._storage_keys.tokens_recent_costs];
        this._key_charge = storage[this._storage_keys.key_charge];
        this._initialized = true;
        setInterval(this.refill.bind(this), 15000);
        return this.refill();
      });
  }

  async handleStorageChange(changes) {
    await this.init();
    if (changes.hasOwnProperty(this._storage_keys.anonymous_tokens_enabled)) {
      this._anonymous_tokens_enabled =
        changes[this._storage_keys.anonymous_tokens_enabled].newValue;
      if (this._anonymous_tokens_enabled) this.refill();
    }
    if (changes.hasOwnProperty(this._storage_keys.tokens)) {
      this._tokens = changes[this._storage_keys.tokens].newValue;
    }
    if (changes.hasOwnProperty(this._storage_keys.tokens_recent_costs)) {
      this._recent_costs =
        changes[this._storage_keys.tokens_recent_costs].newValue;
    }
    if (changes.hasOwnProperty(this._storage_keys.settings)) {
      let new_settings = changes[this._storage_keys.settings].newValue;
      if (new_settings.hasOwnProperty("key")) {
        this._key = new_settings.key;
      } else {
        this._key = null;
      }
    }
    if (changes.hasOwnProperty(this._storage_keys.key_charge)) {
      this._key_charge = changes[this._storage_keys.key_charge].newValue;
    }
  }

  async handleRequestHeaders(details) {
    await this.init();

    // If the user disabled anonymous tokens we'll keep using the remaining ones until empty
    if (!this._anonymous_tokens_enabled && Math.min(...this._recent_costs) > this._tokens.length) {
      return details.requestHeaders;
    }

    let url = new URL(details.url);
    let [headers, cookies] = this._parseRequestHeadersForCookies(
      details.requestHeaders
    );

    let token_header = null;
    if (this._tokens_attach == true && this._recent_costs.length > 0 && url.pathname == "/meta/meta.ger3") {
      token_header = await this._create_token_header();
      if (token_header != null) {
        headers.push(token_header);
      }
    }

    if (!this.urlRequiresKey(url) && (this._anonymous_tokens_enabled || !this._tokens_attach || token_header != null)) {
      cookies = this.hideMetaGerKey(cookies);
    }

    let request_cookies = [];
    for (let key in cookies) {
      let value = cookies[key];
      request_cookies.push(key + "=" + encodeURIComponent(value));
    }
    headers.push({ name: "Cookie", value: request_cookies.join("; ") });
    return headers;
  }

  async handleResponseHeaders(details) {
    await this.init();

    // If the user disabled anonymous tokens we'll keep using the remaining ones until empty
    if (!this._anonymous_tokens_enabled && Math.min(...this._recent_costs) > this._tokens.length) {
      return details.requestHeaders;
    }

    for (let header of details.responseHeaders) {
      if (header.name.match(/set-cookie/i)) {
        let cookies = header.value.split("\n");
        for (let cookie of cookies) {
          let parsed_cookie = new Cookie(cookie);
          if (parsed_cookie.key.match(/cost/i)) {
            if (parsed_cookie.expired) {
              this._tokens_attach = false;
            } else {
              let cost = parseInt(parsed_cookie.value);
              if (!isNaN(cost)) {
                this._tokens_attach = true;
                await this._add_recent_costs(cost);
              }
            }
          } else if (parsed_cookie.key.match(/tokens/i)) {
            try {
              let excess_tokens = JSON.parse(parsed_cookie.value);
              for (let excess_token of excess_tokens) {
                this._tokens.unshift(excess_token);
              }
              await this._store_tokens();
            } catch (error) {
              console.error(error);
            }
          }
        }
      }
    }

    return details.responseHeaders;
  }

  /**
   * Adds an entry to the array of recent
   * costs of the latest search requests
   *
   * @param {int} cost
   */
  async _add_recent_costs(cost) {
    if (isNaN(cost)) return;
    this._recent_costs.unshift(cost);
    while (this._recent_costs.length > 20) {
      this._recent_costs.pop();
    }
    let new_setting = {};
    new_setting[this._storage_keys.tokens_recent_costs] = this._recent_costs;
    return browser.storage.local.set(new_setting).then(() => this.refill());
  }

  /**
   * Removes the Cookie Header from header list
   * and returns a parsed cookie object and remaining headers
   * @param {*} requestHeaders
   */
  _parseRequestHeadersForCookies(requestHeaders) {
    let new_headers = [];

    let cookie_header = null;
    for (let header of requestHeaders) {
      if (header.name.match(/cookie/i)) {
        cookie_header = header;
      } else {
        new_headers.push(header);
      }
    }

    let new_cookies = {};
    if (cookie_header) {
      let cookies = cookie_header.value.split(";");
      for (let cookie of cookies) {
        let cookie_array = cookie.split("=");
        if (cookie_array.length != 2) continue;
        let name = cookie_array[0].trim();
        let value = decodeURIComponent(cookie_array[1].trim());
        new_cookies[name] = value;
      }
    }
    return [new_headers, new_cookies];
  }

  urlRequiresKey(url) {
    let key_paths = ["/keys", "/meta/settings"];
    for (let key_path of key_paths) {
      if (url.pathname.startsWith(key_path)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Removes the MetaGer key from the request
   * and replaces it with the headers for anonymous tokens
   * https://gitlab.metager.de/open-source/MetaGer/-/wikis/Anonymous%20Token%20System
   */
  hideMetaGerKey(cookies) {
    // Set the tokenauthorization cookie instead of the actual key
    let token_authorization = "empty";
    if (this._key_charge > 30) {
      token_authorization = "full";
    } else if (this._key_charge > 0) {
      token_authorization = "low";
    }
    cookies.tokenauthorization = token_authorization;
    delete cookies.key;
    return cookies;
  }

  async _create_token_header() {
    if (this._recent_costs.length == 0) return;
    let cost = this._recent_costs[0];

    if (this._tokens.length < cost) await this.refill();
    if (this._tokens.length < cost) return null;

    let tokens = [];
    for (let i = 0; i < cost; i++) {
      tokens.push(this._tokens.shift());
    }
    return this._store_tokens().then(() => {
      return {
        name: "tokens",
        value: JSON.stringify(tokens)
      }
    });
  }

  /**
   * Persists the saved tokens
   */
  async _store_tokens(key_charge = null) {
    let new_storage = {};
    if (key_charge != null)
      new_storage[this._storage_keys.key_charge] = key_charge;
    new_storage[this._storage_keys.tokens] = this._tokens;
    console.log(this._tokens, this._key_charge);
    return browser.storage.local.set(new_storage);
  }

  /**
   * Refills locally stored tokens
   */
  async refill() {
    let max_cost = 1;
    let required_token_count = 0;

    for (let [index, cost] of this._recent_costs.entries()) {
      if (cost > max_cost) max_cost = cost;
      if (this._recent_costs_number > index) required_token_count += cost;
    }
    required_token_count += max_cost;
    if (
      this._tokens.length >= max_cost ||
      this._key == null ||
      this._anonymous_tokens_enabled == false
    )
      return;

    // Substract existing tokens from required count
    required_token_count = Math.max(
      0,
      required_token_count - this._tokens.length
    );
    if (required_token_count == 0) return;
    required_token_count = Math.min(10, required_token_count);  // We cannot request more than 10 token per request

    let public_key = await fetch(this._api_base + "/token/pubkey").then(
      (response) => response.json()
    );
    let public_key_parsed = new NodeRSA(public_key.pubkey_pem.trim());
    // Generate new tokens
    let tokens = [];
    for (let i = 0; i < required_token_count; i++) {
      let token = crypto.randomUUID();
      let blinded = blind({
        message: token,
        key: public_key_parsed,
      });
      tokens.push({
        token: token,
        blinded: blinded.blinded,
        blinded_r: blinded.r,
        date: public_key.date,
      });
    }

    // Signing request only with the blinded tokens
    // Rest of the data will be this apps secret
    let request_data = {
      key: this._key,
      date: public_key.date,
      blinded_tokens: tokens.map((token) => token.blinded.toString()),
    };

    return fetch(this._api_base + "/token/sign", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(request_data),
    })
      .then((response) => {
        if (response.status == 201) {
          return response.json();
        } else {
          throw new Error("Error signing tokens. The key is probably empty");
        }
      })
      .then((response) => {
        // Save new key charge
        for (let token of tokens) {
          if (response.signed_tokens.hasOwnProperty(token.blinded)) {
            let unblinded_signature = unblind({
              signed: response.signed_tokens[token.blinded],
              key: public_key_parsed,
              r: token.blinded_r,
            });
            if (
              verify({
                unblinded: unblinded_signature,
                message: token.token,
                key: public_key_parsed,
              })
            ) {
              this._tokens.push({
                token: token.token,
                signature: unblinded_signature.toString(),
                date: token.date,
              });
            }
          }
        }
        return this._store_tokens(response.charge);
      });
  }
}
