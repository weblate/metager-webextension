/**
 * Content script that gets injected into calls for all MetaGer websites
 * under https://metager.de and https://metager.org
 * 
 * It will for example remove references/links that help our users finding
 * and installing this web-exension. As the web-extension already is installed
 * we can safely remove those links to clear up the interface.
 */
let pluginButton = document.getElementById("plugin-btn");
if (pluginButton) {
    pluginButton.remove();
}