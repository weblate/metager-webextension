const { Cookie } = require("./Cookie");
const { SettingsManager } = require("./SettingsManager");
const { TokenManager } = require("./TokenManager");

let settingsManager = new SettingsManager();
let tokenManager = new TokenManager();

/**
 * When the user enables/disables storage of settings within this
 * web-extension we'll grab all existing settings and put it into
 * the web-extension or vise versa
 */
browser.storage.onChanged.addListener(async (changes, areaName) => {
  settingsManager.handleStorageChange(changes);
  tokenManager.handleStorageChange(changes);
  if (!browser.cookies) return;
  if (changes.hasOwnProperty("settings_store")) {
    let new_value = changes.settings_store.newValue;
    if (new_value == true) {
      // Setting storage now enabled
      await Cookie.COOKIES_TO_SETTINGS();
    } else if (new_value == false) {
      // Setting storage now disabled. Put all settings into browser cookie storage
      await Cookie.SETTINGS_TO_COOKIES();
    }
  }
});


/**
 * This extension offers two main features:
 * 1. Permanently storing all user settings especially when cookies are being regulary deleted
 * 2. Use homomorphic encryption to anonymize the usage of a MetaGer Key
 * 
 * Those features are implemented using the following listeners which are able
 * to modify the request/response headers from MetaGer.
 */
// Handle Request Headers
browser.webRequest.onBeforeSendHeaders.addListener(
  async (details) => {
    details.requestHeaders = await settingsManager.handleRequestHeaders(
      details
    );
    let headers = await tokenManager.handleRequestHeaders(details);
    return { requestHeaders: headers };
  },
  {
    urls: ["https://metager.org/*", "https://metager.de/*"],
  },
  ["blocking", "requestHeaders"]
);
// Handle Response Headers
browser.webRequest.onHeadersReceived.addListener(
  async (details) => {
    details.responseHeaders = await settingsManager.handleResponseHeaders(
      details
    );

    let headers = await tokenManager.handleResponseHeaders(details);
    return { responseHeaders: headers };
  },
  {
    urls: ["https://metager.org/*", "https://metager.de/*"],
  },
  ["blocking", "responseHeaders"]
);

/**
 * Open settings page when user installs the addon
 * as additional permissions might be required to use
 * all features of this extension.
 * Opening the page on update is not required if the user comes from
 * a version that already has a settings page included
 */

browser.runtime.onInstalled.addListener(details => {
  let open_settings_page = false;

  if (details.reason == "update" && details.previousVersion == "0.0.1.3") {
    open_settings_page = true;
  }

  if (details.reason == "install") {
    open_settings_page = true;
  }

  if (details.temporary == true) {
    open_settings_page = false;
  }

  if (open_settings_page) {
    browser.runtime.openOptionsPage();
  }
})