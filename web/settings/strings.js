// Inserts localized strings into html elements
document.querySelectorAll("[data-text]").forEach(element => {
    let key = element.dataset.text;
    let message = chrome.i18n.getMessage(key);
    if (message)
        element.textContent = message;
})