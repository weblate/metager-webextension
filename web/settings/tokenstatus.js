let key = null;
let charge = 0;
let anonymous_token = 0;

(() => {

    // Check if a key is setup
    browser.storage.sync.get({ settings_settings: {} }).then(storage => {
        if (storage.settings_settings.hasOwnProperty("key")) {
            // Update key charge
            key = storage.settings_settings.key;
            update_key_charge()
        }
        update_anonymous_token();
    })
})();

function update_key_charge() {
    fetch("https://metager.de/keys/api/json/key/" + encodeURIComponent(key), {
        headers: {
            "Cache-Control": "no-cache",
        }
    }).then(response => {
        if (response.ok) {
            return response.json();
        } else {
            throw new Error("Couldn't fetch key status");
        }
    }).then(response => {
        charge = response.charge;
        updateStatus();
    });
}

function update_anonymous_token() {
    browser.storage.local.get({ anonymous_tokens: [] }).then(storage => {
        anonymous_token = storage.anonymous_tokens.length;
        updateStatus();
    });
}

function updateStatus() {
    let key_status_container = document.querySelector("#token-amount");
    let anonymous_token_container = document.querySelector("#anonymous-token-amount");

    let manage_key = document.getElementById("manage-key");
    let setup_key = document.getElementById("setup-key");

    if (key != null) {
        key_status_container.textContent = charge
        anonymous_token_container.textContent = anonymous_token;
        document.querySelector("#anonymous-tokens .status").classList.remove("hidden");
        setup_key.classList.add("hidden");
        manage_key.classList.remove("hidden");
    } else {
        document.querySelector("#anonymous-tokens .status").classList.add("hidden");
        setup_key.classList.remove("hidden");
        manage_key.classList.add("hidden");
    }
}