

// Setting change setting storage
(async () => {
    let store_settings = null;
    let use_anonymous_tokens = null;
    let checkbox_store_settings = document.querySelector("#store-settings-switch");
    let checkbox_anonymous_tokens = document.querySelector("#anonymous-tokens-switch");

    await initializeSettingsState();

    checkbox_store_settings.addEventListener("change", e => {
        updateSettingState(checkbox_store_settings.checked, use_anonymous_tokens);
    });
    checkbox_anonymous_tokens.addEventListener("change", e => {
        updateSettingState(store_settings, checkbox_anonymous_tokens.checked);
    });

    async function verifyPermissions(request = false) {
        let permissionsToRequest = {
            permissions: ["webRequestBlocking"],
            origins: [
                "https://metager.org/*",
                "https://metager.de/*",
            ]
        };

        if (request) {
            return browser.permissions.request(permissionsToRequest);
        } else {
            return browser.permissions.contains(permissionsToRequest);
        }
    }

    async function initializeSettingsState() {
        // Initialize stored settings
        let synced_settings = await browser.storage.sync.get({
            settings_store: true,
            use_anonymous_tokens: false,
        });

        if (synced_settings.settings_store == true || synced_settings.use_anonymous_tokens == true) {
            await verifyPermissions().then(granted => {
                if (granted) {
                    store_settings = synced_settings.settings_store;
                    use_anonymous_tokens = synced_settings.use_anonymous_tokens;
                } else {
                    store_settings = false;
                    use_anonymous_tokens = false;
                }
            });
        } else {
            store_settings = false;
            use_anonymous_tokens = false;
        }

        checkbox_store_settings.checked = store_settings;
        checkbox_anonymous_tokens.checked = use_anonymous_tokens;

        browser.storage.onChanged.addListener((changes, areaName) => {
            if (changes.hasOwnProperty("settings_store")) {
                let new_value = changes.settings_store.newValue;
                checkbox_store_settings.checked = new_value;
                store_settings = new_value;
            }
            if (changes.hasOwnProperty("use_anonymous_tokens")) {
                let new_value = changes.use_anonymous_tokens.newValue;
                checkbox_anonymous_tokens.checked = new_value;
                use_anonymous_tokens = new_value;
            }
        });
    }

    async function updateSettingState(store_settings, use_anonymous_tokens) {
        if (store_settings == true || use_anonymous_tokens == true) {
            return verifyPermissions(true).then(permission_granted => {
                if (permission_granted) {
                    return browser.storage.sync.set({ settings_store: store_settings, use_anonymous_tokens: use_anonymous_tokens });
                } else {
                    checkbox_store_settings.checked = false;
                    checkbox_anonymous_tokens.checked = false;
                    return;
                }
            })
        } else {
            return browser.storage.sync.set({ settings_store: store_settings, use_anonymous_tokens: use_anonymous_tokens });
        }
    }
})();

