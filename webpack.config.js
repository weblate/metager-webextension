const path = require("path");
const { ProvidePlugin } = require("webpack");

module.exports = {
  resolve: {
    fallback: {
      assert: false,
      crypto: require.resolve("crypto-browserify"),
      constants: false,
      vm: false,
      stream: require.resolve("stream-browserify"),
      buffer: require.resolve("buffer/"),
    },
  },
  entry: {
    app: "./build/js/app.js",
    removeUnusedContent: "./build/js/removeUnusedContent.js",
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, "js"),
  },
  devtool: "source-map",
  watch: true,
  watchOptions: {
    ignored: ["/node_modules"],
  },
  plugins: [
    new ProvidePlugin({
      Buffer: ["buffer", "Buffer"],
      process: "process/browser",
    }),
  ],
};
